flask>=1.1.1
youtube-dl>=2019.10.22
flask_socketio>=4.2.1
eventlet>=0.25.1
python-dotenv>=0.10.3
requests>=2.22.0
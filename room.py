# -*- encoding: utf-8 -*-

import time
import threading
import socketio

from youtube_dl import DownloadError
from youtube_dl import YoutubeDL

ydl_opts = {
    "skip_download": 1,
    "noplaylist": 1,
    "format": "bestaudio",
    "subtitleslangs": ["en"],
    "writesubtitles": 1
}

class Room:

    def set_next(self):
        if not len(self.__list):
            print("[DEBUG] ({}) Playlist empty! Emitting Stop command.".format(self.__id))
            self.__playing = False
            self.__paused = False
            self.__sio.emit("stop", room=self.__id)
        else:
            print("[DEBUG] ({}) Setting next song!".format(self.__id))
            self.__current_song = self.__list.pop(0)
            if self.__playing is False:
                self.__playing = True
            self.__start_time = time.time()
            self.__play_time = 0
            # self.__current_stream = self.__current_song["url"]
            # self.__current_song["url"] = "/room/{}/stream".format(self.__id)
            self.__sio.emit("play", {"sinfo": self.__current_song, "st": self.__start_time, "pt": 0, "paused": self.__paused}, room=self.__id)
            self.__sio.emit("queue", self.get_queue())

    def pause(self):
        print("[DEBUG] ({}) Pausing!".format(self.__id))
        self.__paused = True
        self.__play_time = time.time() - self.__start_time + self.__play_time
        self.__sio.emit("pause", room=self.__id)

    def resume(self):
        print(f"[DEBUG] ({self.__id}) Resuming!")
        self.__paused = False
        self.__start_time = time.time()
        self.__sio.emit("resume", self.get_current_info(), room=self.__id)

    """
    Return the current song with syncing information
    """

    def get_current_info(self):
        if self.__paused:
            return {"sinfo": self.__current_song, "st": self.__start_time, "pt": int(round(self.__play_time)) * 1000, "paused": self.__paused}
        return {"sinfo": self.__current_song, "st": self.__start_time, "pt": int(round(time.time() - self.__start_time + self.__play_time)) * 1000, "paused": self.__paused}

    def send_message(self, user, message, server=False):
        if user in self.__users:
            self.__sio.emit(
                "chatMessage", {"user": self.__users[user], "msg": message}, room=self.__id)
        if server:
            self.__sio.emit(
                "chatMessage", {"user": {"sid": "SYS", "name": "Server"}, "msg": message}, room=self.__id)

    def join(self, user):
        if(self.__owner == None):
            self.__owner = user
        print("[DEBUG] ({}) Adding user {} to Room!".format(
            self.__id, "guest-" + user[:5]))
        self.__users[user] = {
            "sid": user,
            "name": "guest-" + user[:5]
        }

    def leave(self, user):
        print("[DEBUG] ({}) User {} left the Room!".format(self.__id, user))
        if user in self.__users:
            del self.__users[user]
        if user == self.__owner:
            if(len(self.__users) < 1):
                self.__owner = None
            else:
                self.__owner = self.__users[0]

    def get_users(self):
        return list(self.__users)

    def get_queue(self):
        return self.__list

    def add_song(self, url):
        print("[DEBUG] ({}) Adding URL {} to Queue!".format(self.__id, url))
        self.__sio.sleep()
        try:
            songObject = self.__ytdl.extract_info(url)
            objectToAdd = {
                "url": songObject["fragment_base_url"] if "fragment_base_url" in songObject else songObject["url"],
                "artist": songObject["artist"] or songObject["uploader"],
                "song": {
                    "title": songObject["alt_title"] or songObject["title"],
                    "album": songObject["album"] or None,
                    "duration": songObject["duration"],
                    "cover": songObject["thumbnail"],
                    "link": songObject["webpage_url"]
                }
            }

            if songObject["requested_subtitles"] is not None:
                objectToAdd["song"]["lyrics"] = songObject["requested_subtitles"]["en"]["url"]
            else:
                objectToAdd["song"]["lyrics"] = False
            self.__list.append(objectToAdd)
            if not self.__playing:
                self.set_next()
            else:
                self.__sio.emit("queue", self.get_queue(), room=self.__id)
        except DownloadError:
            self.__error("Invalid URL!")
            print("[DEBUG] Invalid URL!")

    def add(self, url):
        with self.__add_song_lock:
            self.__sio.start_background_task(self.add_song, url)
            print("U wanna prank me bro?")

    def is_owner(self, sessid):
        return self.__owner == sessid

    def is_playing(self):
        return self.__playing

    def is_paused(self):
        return self.__paused
    
    def _get_current_stream(self):
        return self.__current_stream

    def __error(self, message):
        self.__sio.emit('error', {"message": message})

    def __init__(self, owner, room, sio):
        print("Creating room with owner {}".format(owner))
        self.__add_song_lock = threading.Lock()
        self.__id = room
        self.__owner = owner
        self.__current_stream = None
        self.__sio = sio
        self.__ytdl = YoutubeDL(ydl_opts)
        self.__start_time = None
        self.__end_time = None
        self.__current_time = None
        self.__play_time = 0
        self.__current_timetstamp = None
        self.__current_song = None
        self.__playing = False
        self.__paused = False
        self.public = False
        self.__users = dict()
        self.__list = list()

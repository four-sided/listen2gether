# listen2gether ![](https://img.shields.io/badge/python%20version-3+-brightgreen.svg?style=flat) [![GitLab pipeline](https://img.shields.io/gitlab/pipeline/four-sided/listen2gether)](https://gitlab.com/four-sided/listen2gether/builds)
Listen to Music together. Now. Simple.

This project tries to get people together and listen together (who would have guessed)
With awesome features like
*  Lyrics (if the video got subtitles)
*  A chat! (Until you refresh your browser and wipe all the history)
*  Bugs! ( :) )
*  A website design for mobile and desktop view! (One feature that actually is true)
*  Playlist import (from Youtube!)
*  Cool and fancy storing of your volume! (In your LocalStorage!)
*  A heck long list of features! (This list isn't even so long...)

So clone it now, run `pip install -r requirements.txt && ./run_dev.sh` and experience the fantastic ability to listen together (If your local machine is connected to the internet and Google won't rage at you and block requests.)

© four-sided
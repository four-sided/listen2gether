let play = false;
let pause = false;
let song = {};
let player = document.createElement("audio");
let socket = io();

let latency = 0;
let lastPing;
let failedBeats = 0;
let pingInterval;
let pinged = false;

let timeTimeoutCode = 0;
let currentSongDuration = 0;

let latestScroll = 0;

let ifDisconnected = false;

let parser = new WebVTT.Parser(window, WebVTT.StringDecoder());
let subtitles = []
let subindex = 0;

player.addEventListener("timeupdate", (time) => {
  //console.log(player.currentTime)
  if(subtitles.length > subindex)
    if (subtitles[subindex].time < (player.currentTime + .2)){
      if(subindex > 0){
        editCurrentNotification(subtitles[subindex].text, 
          (subtitles.length > subindex + 1) ? subtitles[subindex + 1].text : "[END]", 
          "info", 
          (subtitles.length > subindex + 1) ? 0 : ((currentSongDuration - subtitles[subindex].time) * 1000 + 200));
      } else {
        addNotification(subtitles[subindex].text,
          (subtitles.length > subindex + 1) ? subtitles[subindex + 1].text : "[END]",
          "info",
          (subtitles.length > subindex + 1) ? 0 : ((currentSongDuration - subtitles[subindex].time) * 1000 + 200));
      }
    subindex++;
    }
})

function getSubtitles(url){
  subtitles = []
  let xhr = new XMLHttpRequest();
  xhr.open("GET", url);
  xhr.addEventListener("load", () => {
    let index = 0
    parser.oncue = function(cue){
      subtitles[index] = {
        "time": cue.startTime,
        "text": cue.text
      }
      index++;
    }
    parser.parse(xhr.responseText);
    parser.flush();
  });
  xhr.send();
}

socket.on('connect', () => {
  console.log("connected");
  if (ifDisconnected) editCurrentNotification("Connected", "", "success", 1000);
  socket.emit("join", roomid);

  if (pingInterval == undefined) {
    pingInterval = setInterval(function() {
      if (!pinged) {
        lastPing = Date.now();
        socket.emit('beat');
        console.log('ping');
        pinged = true;
        failedBeats = 0;
      }else{
        failedBeats++;
        console.log('No heartbeat recieved...');
        if(failedBeats > 1){
          console.log('Reinitiating connection...');
          socket.disconnect();
          socket.connect();
          if(failedBeats > 15) {
            window.location.reload();
          }
        }
      }
    }, 5000);
  }
  socket.emit("queue");
});

socket.on("joined", owner => {
  console.log("Admin?", owner);
})

socket.on('disconnect', () => {
    ifDisconnected = true;
    console.log("disconnected");
    addNotification("Disconnected", "trying to reconnect", "error", 0);
});

socket.on('beat', () => {
  latency = Math.floor(Date.now()) - lastPing;
  console.log("Latency:", latency);
  pinged = false;
})

socket.on('play', (data) => {
  subindex = 0;
  console.log('Play recieved');
  console.log(data);
  play = true;
  player.src = data.sinfo.url;
  currentSongDuration = data.sinfo.song.duration;
  pause = data.paused;
  if(data.sinfo.song.lyrics)
    getSubtitles(data.sinfo.song.lyrics)
  else
    subtitles = []
  if(!data.paused){
    startTimer();
    document.getElementById("playPause").src = "/static/icons/pause-circle.svg";
    // let time = Math.floor(Date.now()) - data.st;
    console.log(latency / 1000);
    player.currentTime = latency / 1000 + data.pt / 1000;
    console.log("START AT " + player.currentTime + "!");
    player.play();
  } else {
    console.log(latency / 1000);
    player.currentTime = latency / 1000 + data.pt / 1000;
    console.log("START AT " + player.currentTime + "!");
    player.load();
    document.getElementById("playPause").src = "/static/icons/play-circle.svg";
  }
  updateSongInfo(data.sinfo);
});

socket.on('pause', () => {
  if(!play) return;
  pause = true;
  console.log("PAUSE AT " + player.currentTime + "!");
  player.pause();
  clearTimeout(timeTimeoutCode);
  document.getElementById("playPause").src = "/static/icons/play-circle.svg";
});

socket.on('resume', (data) => {
  if (!play) return;
  // player.src = data.curl;
  pause = false;
  console.log(data);
  player.currentTime = ((latency / 1000)) + data.pt / 1000 || 0;
  console.log("RESUME AT " + player.currentTime + "!");
  player.play();
  startTimer();
  document.getElementById("playPause").src = "/static/icons/pause-circle.svg";
});

socket.on('chatMessage', (data) => {
  console.log(`Recieved chat message; (${data.user.name}) > ${data.msg}`);
  let field = document.getElementById("chatfield");
  let who = 'other';
  if (data.user.sid === socket.id) {
    who = 'self';
  }
  if (data.user.sid === 'SYS') {
    who = 'system';
  }
  let id = 'msg-' + Math.floor(Math.random() * Math.floor(10000000));
  field.innerHTML += `<p><span class="username ${who}" >${data.user.name}</span><span id="${id}"></span></p>`;
  document.getElementById(id).innerText = data.msg;
  field.scroll({
    top: field.scrollHeight,
    behavior: "smooth"
  });
});

socket.on('error', data => console.log('Error:', data.message));

player.addEventListener("ended", () => {
  console.log("Song ended, time for the NEXT one.");
  socket.emit("next");
})

window.addEventListener('load', () => {
  document.getElementsByClassName("share")[0].addEventListener('click', () => {
    let url = window.location.href;
    let area = document.createElement("input");
    area.type = "text";
    area.value = url;
    document.body.appendChild(area);

    area.select();
    area.setSelectionRange(0, 99999);

    document.execCommand("copy");
    area.remove();
    console.log("text copied");
  });

  document.addEventListener('scroll', (e) => {
    if (e.target.scrollingElement.scrollTop < 100) {
      for (let e of document.getElementsByClassName("header")) {
        e.classList.remove("scroll");
      }
    } else {
      for (let e of document.getElementsByClassName("header")) {
        e.classList.add("scroll");
      }
    }
    latestScroll = e.target.scrollingElement.scrollTop;
  });

  document.getElementById("chatInput").addEventListener('keyup', (e) => {
    if (e.key === 'Enter') {
      sendChat();
    }
  });

  document.getElementById("chatSend").addEventListener('click', (e) => {
    sendChat();
  });

  document.getElementById("searchInput").addEventListener('keyup', (e) => {
    if (e.key === 'Enter') {
      sendSearch();
    }
  });

  document.getElementById("searchSend").addEventListener('click', (e) => {
    sendSearch();
  });

  document.getElementById("playPause").addEventListener('click', (e) => {
    if (!pause) {
      if (!play) {
        e.target.src = "/static/icons/pause-circle.svg";
        socket.emit("play");
      } else {
        e.target.src = "/static/icons/play-circle.svg";
        player.pause();
        socket.emit("pause", player.currentTime);
      }
    } else {
      e.target.src = "/static/icons/pause-circle.svg";
      socket.emit("resume");
    }
  });

  document.getElementById("volumeSlider").addEventListener("input", (e) => {
    document.getElementById("volumeInfo").innerText = e.target.value + "%";
    player.volume = e.target.value / 100;
    window.localStorage.setItem("volume", player.volume);
  });
  player.volume = window.localStorage.getItem("volume") || .5;
  document.getElementById("volumeInfo").innerText = player.volume * 100 + "%";
  document.getElementById("volumeSlider").value = player.volume * 100;
});


function sendChat() {
  const chatInput = document.getElementById("chatInput");
  if (chatInput.value) {
    const msg = chatInput.value;
    socket.emit("chatMessage", msg);
    console.log("Sent message: " + msg);
    chatInput.value = '';
  }
}

function sendSearch() {
  const searchInput = document.getElementById("searchInput");
  if (searchInput.value) {
    const msg = searchInput.value;
    socket.emit("add", msg);
    console.log("Sent ADD: " + msg);
    searchInput.value = '';
  }
}

function updateSongInfo(sinfo) {
  document.getElementById('songName').innerText = sinfo.song.title;
  document.getElementById('songName').href = sinfo.song.link;
  document.getElementById('songArtist').innerText = sinfo.artist;
  document.getElementsByClassName('thumbnail')[0].src = sinfo.song.cover;
  document.getElementById('songDuration').innerText = Math.floor(player.currentTime / 60) + ':' + (Math.floor(((player.currentTime % 60)).toFixed(0)) < 10 ? 0 : "") + Math.floor(((player.currentTime % 60)).toFixed(0)) + ' / ' + Math.floor(currentSongDuration / 60) + ':' + (Math.floor(((currentSongDuration % 60)).toFixed(0)) < 10 ? 0 : "") + Math.floor(((currentSongDuration % 60)).toFixed(0));
}

function startTimer(){
  clearInterval(timeTimeoutCode);
  timeTimeoutCode = setInterval(function() {
    document.getElementById('songDuration').innerText = Math.floor(player.currentTime / 60) + ':' + (Math.floor(((player.currentTime % 60)).toFixed(0)) < 10 ? 0 : "") + Math.floor(((player.currentTime % 60)).toFixed(0)) + ' / ' + Math.floor(currentSongDuration / 60) + ':' + (Math.floor(((currentSongDuration % 60)).toFixed(0)) < 10 ? 0 : "") + Math.floor(((currentSongDuration % 60)).toFixed(0));
  }, 500);
}


socket.on('queue', function (queue){
  let htmlContent = "<p>Currently empty :(</p>";
  console.log(queue);

  if(queue.length > 0)
    htmlContent = "";
  for(let i = 0; i < queue.length; i++){
    let s = queue[i];
    console.log(s);
    htmlContent += `<div class="queueSong">
                        <img src="${s.song.cover}">
                        <span>
                            <a class="title" href="${s.song.link}" target="_blank">${s.song.title}</a>
                            <p class="artist">${s.artist}</p>
                            <p class="duration">${Math.floor(s.song.duration / 60) + ':' + (Math.floor(((s.song.duration % 60)).toFixed(0)) < 10 ? 0 : "") + Math.floor(((s.song.duration % 60)).toFixed(0))}</p>
                        </span>
                        <div>
                            <h2 class="index">#${i + 1}</h2>
                        </div>
                    </div>`;
  }
  document.getElementById("queue").innerHTML = htmlContent;
});

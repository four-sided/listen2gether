let notificationStack = [];

let occupied = false;
let loaded = false;

let notificationContainer;
let currentNotification;


window.addEventListener("load", () => {
    notificationContainer = document.getElementById("notificationContainer");
    loaded = true;
    if(notificationStack.length > 0)
        showNotification(notificationStack.pop());
});

function showNotification(notification){
    console.log("Showing new notification.");
    let node = document.createElement("div");
    let title = document.createElement("h2");
    let content = document.createElement("p");
    node.classList.add("notification", notification.type);
    title.innerText = notification.title;
    content.innerText = notification.content;
    node.appendChild(title);
    node.appendChild(content);
    let result = notificationContainer.appendChild(node);
    notificationContainer.classList.add("show");
    occupied = true;
    currentNotification = result;
    if(notification.duration > 0)
        setTimeout(hideCurrentNotification, notification.duration);
    if(typeof(callback) == Function){
        callback(result);
    }
}

function hideCurrentNotification() {
    notificationContainer.classList.remove("show");
    notificationContainer.innerHTML = "";
    currentNotification = undefined;
    occupied = false;
    if(notificationStack.length > 0)
        setTimeout(showNotification, 200, (notificationStack.pop()));
}

function editCurrentNotification(title=undefined,content=undefined,type=undefined,duration=undefined){
    if(title != undefined)
        currentNotification.firstChild.innerText = title;
    if (content != undefined)
        currentNotification.lastChild.innerHTML = content;
    if(type != undefined){
        currentNotification.classList.replace(currentNotification.classList[1], type);
    }
    if (duration != undefined && duration > 0)
        setTimeout(hideCurrentNotification, duration);
}

function addNotification(title, content, type="info", duration = 3000, dissmisable = true, callback = undefined) {
    notificationStack.push({
        "title": title,
        "content": content,
        "type": type,
        "duration": duration,
        "dissmisable": dissmisable,
        "callback": callback
    });

    if(loaded && !occupied && notificationStack.length == 1){
        showNotification(notificationStack.pop());
    }
}

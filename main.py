# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import os
import random

import flask_socketio as sio
from dotenv import load_dotenv
from flask import Flask, render_template, make_response, session, request
from requests import get
import flask

from room import Room

load_dotenv()

rooms = {}

app = Flask(__name__)
app.config['SECRET_KEY'] = random.getrandbits(32)
socketio = sio.SocketIO(app, async_handlers=True)


# Hey, do you wanna see my sock collection? ( ͡° ͜ʖ ͡°)

@socketio.on('connect')
def connected():
    return True

@socketio.on('disconnect')
def disconnect():
    if "last_room" in session and session["last_room"] is not None:
        sio.leave_room(session["last_room"])
        rooms[session["last_room"]].send_message(None, "User guest-{} left the room!".format(request.sid[:5]), True)
        rooms[session["last_room"]].leave(request.sid)
    return True


@socketio.on('join')
def on_join(room):
    if "last_room" in session and session["last_room"] is not None:
        return None, session["last_room"]
    if not room in rooms:
        rooms[room] = Room(request.sid, room, socketio)
    sio.join_room(room)
    rooms[room].join(request.sid)
    session["last_room"] = room
    rooms[session["last_room"]].send_message(None, "User guest-{} joined the room!".format(request.sid[:5]), True)
    sio.emit("joined", rooms[room].is_owner(request.sid))


@socketio.on('leave')
def on_leave(room):
    if request.sid in rooms[room].users:
        sio.leave_room(room)

@socketio.on('next')
def on_next():
    if "last_room" in session and session["last_room"] in rooms:
        r = rooms[session["last_room"]]
        r.set_next()

@socketio.on('play')
def on_play():
    if "last_room" in session and session["last_room"] in rooms:
        r = rooms[session["last_room"]]
        if r.is_owner(request.sid):
            if not r.is_playing():
                if not r.is_paused():
                    r.set_next()
                else:
                    sio.emit("play", r.get_current_info())
            else:
                sio.emit("play", r.get_current_info())
        else:
            sio.emit("play", r.get_current_info())


@socketio.on("adminMessage")
def on_achat(msg):
    if "last_room" in session and session["last_room"] in rooms:
        r = rooms[session["last_room"]]
        r.send_message(request.sid, msg, True)


@socketio.on("chatMessage")
def on_chat(msg):
    if "last_room" in session and session["last_room"] in rooms:
        r = rooms[session["last_room"]]
        r.send_message(request.sid, msg)


@socketio.on('pause')
def on_pause(play_time):
    print("[DEBUG] PAUSE")
    print(play_time)
    if "last_room" in session and session["last_room"] in rooms:
        r = rooms[session["last_room"]]
        # if r.is_owner(request.sid):  # TODO: Different sids; idk why
        r.pause()


@socketio.on('resume')
def on_resume():
    print("[DEBUG] RESUME")
    if "last_room" in session and session["last_room"] in rooms:
        r = rooms[session["last_room"]]
        # if r.is_owner(request.sid):  # TODO: Different sids; idk why
        r.resume()


@socketio.on('add')
def on_add(url):
    if "last_room" in session and session["last_room"] in rooms:
        rooms[session["last_room"]].add(url)

@socketio.on('beat')
def beat():
    sio.emit('beat')
    # print("[BEAT] Recieved heartbeat.")

@socketio.on('queue')
def squeue():
    if "last_room" in session and session["last_room"] in rooms:
        sio.emit('queue', rooms[session["last_room"]].get_queue())

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/room/<roomid>/stream')
def stream(roomid):
    if roomid in rooms:
        r = get(rooms[roomid]._get_current_stream(), stream=True)
        def generator():
            yield ''
            for data in r.iter_content(10*1024):
                yield data
        req = flask.Response(generator(), content_type=r.headers["Content-Type"])
        req.headers["Cache-Control"] = "no-cache"
        return req
    return None

@app.route('/room/<roomid>/queue')
def queue(roomid):
    if roomid in rooms:
        return flask.jsonify(rooms[roomid].get_queue())
    else:
        return flask.jsonify({"error": "Room does not exist!", "code": 404})    


@app.route('/room/<roomid>')
def room(roomid):
    r = make_response(render_template('room.html', roomid=roomid))
    r.headers.set('Access-Control-Allow-Origin', '*')
    return r


port = 5000 if 'PORT' not in os.environ else os.environ['PORT']

if __name__ == '__main__':
    socketio.async_mode = None
    socketio.run(app, '0.0.0.0', port)
